package edu.app.web.mb;




import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.app.business.UserLocal;
import edu.app.persistance.AdminRecrutement;

@ManagedBean
@ViewScoped
public class AdminBean {
	@EJB
	private UserLocal userLocal;
	
	private AdminRecrutement adminRecrutement = new AdminRecrutement();
	private List<AdminRecrutement> admins;
	
	private boolean formDisplayed = false;
	
	public AdminBean() {
	}
	
	@PostConstruct
	public void init(){
		admins = userLocal.findAllrecrutement();
	}

	public String doSaveOrUpdate(){
		String navigateTo = null;
	userLocal.create(adminRecrutement);
		admins = userLocal.findAllrecrutement();
		formDisplayed = false;
		return navigateTo;
	}
	
	public String doNew(){
		String navigateTo = null;
		formDisplayed = true;
		return navigateTo;
	}
	
	public String doCancel(){
		String navigateTo = null;
		adminRecrutement = new AdminRecrutement();
	admins = userLocal.findAllrecrutement();
		formDisplayed = false;
		return navigateTo;
	}
	
	public String doDelete(){
		String navigateTo = null;
	userLocal.remove(adminRecrutement);
		admins = userLocal.findAllrecrutement();
		formDisplayed = false;
		return navigateTo;
	}

	

	

	public AdminRecrutement getAdminRecrutement() {
		return adminRecrutement;
	}

	public void setAdminRecrutement(AdminRecrutement adminRecrutement) {
		this.adminRecrutement = adminRecrutement;
	}

	public List<AdminRecrutement> getAdmins() {
		return admins;
	}

	public void setAdmins(List<AdminRecrutement> admins) {
		this.admins = admins;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}
	
	
	

}


