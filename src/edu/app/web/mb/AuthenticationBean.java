package edu.app.web.mb;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.app.business.UserLocal;

import edu.app.persistance.User;




@ManagedBean(name="authBean")
@SessionScoped
public class AuthenticationBean implements Serializable{
	
	@EJB
	private UserLocal userLocal;
	
	private User user = new User();
	private boolean loggedIn = false;
	private String userType = "";

	private static final long serialVersionUID = 6710404278650523921L;
	public AuthenticationBean() {
	}
	
	public String login(){
		String navigateTo = null;
		User found = userLocal.GetUserByLoginAndPwd(user.getLogin(), user.getPassword());
		if (found != null) {
			user = found;
			loggedIn = true;
			if (user instanceof User) {
				userType = "User";
				navigateTo = "/pages/admin/home";
			}
		} else {
			FacesMessage message = new FacesMessage("Bad credentials!");
			FacesContext.getCurrentInstance().addMessage("login_form:login_submit", message);
			loggedIn = false;
			navigateTo = null;
		}
		return navigateTo;
	}
	
	public String logout(){
		String navigateTo = null;
		loggedIn = false;
		user = new User();
		userType = "";
		navigateTo = "/welcome";
		return navigateTo;
	}

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
	
	
	
	

}
