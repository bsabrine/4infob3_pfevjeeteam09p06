package edu.app.web.mb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import edu.app.business.AnotherEmailSenderLocal;
import edu.app.business.CandidatServiceLocal;
import edu.app.business.CertificatLocal;
import edu.app.business.DiplomeLocal;
import edu.app.business.ExperienceLocal;
import edu.app.business.FormationLocal;
import edu.app.business.RecrutementCandidateServiceLocal;
import edu.app.business.TraineshipLocal;
import edu.app.persistance.Candidat;
import edu.app.persistance.Certificat;
import edu.app.persistance.Diplome;
import edu.app.persistance.Experience;
import edu.app.persistance.Formation;
import edu.app.persistance.RecrutementCandidate;
import edu.app.persistance.Trainship;


@ManagedBean
@ViewScoped
public class FiltreFrameBean {

	@EJB
	CandidatServiceLocal local;
	@EJB
	DiplomeLocal diplome;
	@EJB
	FormationLocal formation;
	@EJB
	CertificatLocal certificat;
	@EJB
	ExperienceLocal experience;
	@EJB
	TraineshipLocal trainship;
	@EJB
	AnotherEmailSenderLocal mail;
	@EJB
	RecrutementCandidateServiceLocal recrut;
	
	private boolean formDisplayed = false;
	private List<SelectItem> selectItemsForFormation;
	private List<SelectItem> selectItemsForTrainship;
	private List<SelectItem> selectItemsForCertificat;
	private List<SelectItem> selectItemsForExperience;
	private List<SelectItem> selectItemsForDiplome;
	private int selectedFormationId = -1;
	private int selectedTrainshipId = -1;
	private int selectedCertificatId = -1;
	private int selectedExperienceId = -1;
	private int selectedDiplomeId = -1;
	private Candidat candidat = new Candidat() ;
	private RecrutementCandidate recrutementCandidate = new RecrutementCandidate();
	@PostConstruct
	public void init(){
		candidates = local.findAll();
		List<Formation> formations = formation.findAll();
		selectItemsForFormation = new ArrayList<SelectItem>(formations.size());
		for(Formation formation:formations){
			selectItemsForFormation.add(new SelectItem(formation.getId_forma(),formation.getType_forma()));
		}
		List<Trainship> trainships = trainship.findAll();
		selectItemsForTrainship = new ArrayList<SelectItem>(trainships.size());
		for(Trainship trainship:trainships){
			selectItemsForTrainship.add(new SelectItem(trainship.getId_tr(),trainship.getPosition_t()));
		}
		
		List<Experience> experiences = experience.findAll();
		selectItemsForExperience = new ArrayList<SelectItem>(experiences.size());
		for(Experience experience:experiences){
			selectItemsForExperience.add(new SelectItem(experience.getId_experience(),experience.getPosition_exp()));
		}
		List<Certificat> certificats = certificat.findAll();
		selectItemsForCertificat = new ArrayList<SelectItem>(certificats.size());
		for(Certificat certificat:certificats){
			selectItemsForCertificat.add(new SelectItem(certificat.getId_cert(),certificat.getType_cert()));
		}
		List<Diplome> diplomes = diplome.findAll();
		selectItemsForDiplome = new ArrayList<SelectItem>(diplomes.size());
		for(Diplome diplome:diplomes){
			selectItemsForDiplome.add(new SelectItem(diplome.getId_dip(),diplome.getType_dip()));
		}
	}

	private Formation formation2;
	private List<Candidat> candidates;
	private List<Candidat> selectedCandidates;
	
	private List<Diplome> diplomes;
	private Diplome selectedDiplome = new Diplome();
	
	private List<Formation> formations;
	private Formation selectedFormation = new Formation();
	
	private List<Certificat> certificats;
	private Certificat selectedCertificat = new Certificat();
	
	private List<Experience> experiences;
	private Experience selectedExperience = new Experience();
	
	private List<Trainship> traineships;
	private Trainship selectedTrainship = new Trainship();
	

public List<Candidat> getCandidates() {
	return candidates;
}

public void setCandidates(List<Candidat> candidates) {
	this.candidates = candidates;
}

public List<Candidat> getSelectedCandidates() {
	return selectedCandidates;
}

public void setSelectedCandidates(List<Candidat> selectedCandidates) {
	this.selectedCandidates = selectedCandidates;
}

public List<Diplome> getDiplomes() {
	return diplomes;
}

public void setDiplomes(List<Diplome> diplomes) {
	this.diplomes = diplomes;
}

public Diplome getSelectedDiplome() {
	return selectedDiplome;
}

public void setSelectedDiplome(Diplome selectedDiplome) {
	this.selectedDiplome = selectedDiplome;
}

public List<Formation> getFormations() {
	formations= formation.findAll();
	return formations;
}

public void setFormations(List<Formation> formations) {
	this.formations = formations;
}

public Formation getSelectedFormation() {
	return selectedFormation;
}

public void setSelectedFormation(Formation selectedFormation) {
	this.selectedFormation = selectedFormation;
}

public List<Certificat> getCertificats() {
	return certificats;
}

public void setCertificats(List<Certificat> certificats) {
	this.certificats = certificats;
}

public Certificat getSelectedCertificat() {
	return selectedCertificat;
}

public void setSelectedCertificat(Certificat selectedCertificat) {
	this.selectedCertificat = selectedCertificat;
}

public List<Experience> getExperiences() {
	return experiences;
}

public void setExperiences(List<Experience> experiences) {
	this.experiences = experiences;
}

public Experience getSelectedExperience() {
	return selectedExperience;
}

public void setSelectedExperience(Experience selectedExperience) {
	this.selectedExperience = selectedExperience;
}

public List<Trainship> getTraineships() {
	traineships= trainship.findAll();
	return traineships;
}

public void setTraineships(List<Trainship> traineships) {
	this.traineships = traineships;
}

public Trainship getSelectedTrainship() {
	return selectedTrainship;
}

public void setSelectedTrainship(Trainship selectedTrainship) {
	this.selectedTrainship = selectedTrainship;
}

public String actionPerformed() {
	candidates.clear();
	candidates= local.findFilter(selectedFormationId, selectedTrainshipId, selectedCertificatId, selectedExperienceId, selectedDiplomeId);

	return null;
}


public void ajouter() {
	recrutementCandidate.setCandidat(candidat);
    recrut.create(recrutementCandidate);
	
mail.sendMail(candidat.getMail(), "Interview!", "BHir Sir / Madam you are accepting our recruitment (esprit) and You can contact us for technical recruitment pass this address 18/04/2013 Tunis void of cartage");

	}

public Formation getFormation2() {
	return formation2;
}

public void setFormation2(Formation formation2) {
	this.formation2 = formation2;
}

public boolean isFormDisplayed() {
	return formDisplayed;
}

public void setFormDisplayed(boolean formDisplayed) {
	this.formDisplayed = formDisplayed;
}

public List<SelectItem> getSelectItemsForFormation() {
	return selectItemsForFormation;
}

public void setSelectItemsForFormation(List<SelectItem> selectItemsForFormation) {
	this.selectItemsForFormation = selectItemsForFormation;
}

public int getSelectedFormationId() {
	return selectedFormationId;
}

public void setSelectedFormationId(int selectedFormationId) {
	this.selectedFormationId = selectedFormationId;
}

public List<SelectItem> getSelectItemsForTrainship() {
	return selectItemsForTrainship;
}

public void setSelectItemsForTrainship(List<SelectItem> selectItemsForTrainship) {
	this.selectItemsForTrainship = selectItemsForTrainship;
}

public int getSelectedTrainshipId() {
	return selectedTrainshipId;
}

public void setSelectedTrainshipId(int selectedTrainshipId) {
	this.selectedTrainshipId = selectedTrainshipId;
}

public List<SelectItem> getSelectItemsForCertificat() {
	return selectItemsForCertificat;
}

public void setSelectItemsForCertificat(List<SelectItem> selectItemsForCertificat) {
	this.selectItemsForCertificat = selectItemsForCertificat;
}

public int getSelectedCertificatId() {
	return selectedCertificatId;
}

public void setSelectedCertificatId(int selectedCertificatId) {
	this.selectedCertificatId = selectedCertificatId;
}

public List<SelectItem> getSelectItemsForExperience() {
	return selectItemsForExperience;
}

public void setSelectItemsForExperience(List<SelectItem> selectItemsForExperience) {
	this.selectItemsForExperience = selectItemsForExperience;
}

public List<SelectItem> getSelectItemsForDiplome() {
	return selectItemsForDiplome;
}

public void setSelectItemsForDiplome(List<SelectItem> selectItemsForDiplome) {
	this.selectItemsForDiplome = selectItemsForDiplome;
}

public int getSelectedDiplomeId() {
	return selectedDiplomeId;
}

public void setSelectedDiplomeId(int selectedDiplomeId) {
	this.selectedDiplomeId = selectedDiplomeId;
}

public int getSelectedExperienceId() {
	return selectedExperienceId;
}

public void setSelectedExperienceId(int selectedExperienceId) {
	this.selectedExperienceId = selectedExperienceId;
}

public Candidat getCandidat() {
	return candidat;
}

public void setCandidat(Candidat candidat) {
	this.candidat = candidat;
}

public RecrutementCandidate getRecrutementCandidate() {
	return recrutementCandidate;
}

public void setRecrutementCandidate(RecrutementCandidate recrutementCandidate) {
	this.recrutementCandidate = recrutementCandidate;
}

}