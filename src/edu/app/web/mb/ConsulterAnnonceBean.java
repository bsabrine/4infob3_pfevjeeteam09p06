package edu.app.web.mb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import edu.app.business.AnnonceServiceLocal;
import edu.app.persistance.Annonce;

@ManagedBean
@SessionScoped
public class ConsulterAnnonceBean {
	
	
	@EJB
	AnnonceServiceLocal offre;
	
		
	private boolean formDisplayedOff = false;
	public boolean isFormDisplayedOff() {
		return formDisplayedOff;
	}
	public void setFormDisplayedOff(boolean formDisplayedOff) {
		this.formDisplayedOff = formDisplayedOff;
	}
	
	
	Annonce annonce= new Annonce();
	private List<Annonce> annonces;
	private Annonce selectedAnnonce; 
	
	public ConsulterAnnonceBean() {
		annonces = new ArrayList<Annonce>();
		 populateRandomAnnonces(annonces, 50);
	}



private void populateRandomAnnonces(List<Annonce> list, int i) {
	 for(int i1 = 0 ; i1 <  i ; i1++)  
         list.add(new Annonce(annonce.getId(), annonce.getDate(), annonce.getContenu())); 
	}
public List<Annonce> getAnnonces() {
	annonces= offre.findAll();
	return annonces;
}
public void setAnnonces(List<Annonce> annonces) {
	this.annonces = annonces;
}

	
	
	
	
	
	
	
	public Annonce getAnnonce() {
		return annonce;
	}
	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}
	
	
	
	public void sobb(AjaxBehaviorEvent event, Annonce annonce){
		System.out.println("nsobb");
		setSelectedAnnonce(annonce);
	}

public String addToOf(){
	offre.create(annonce);
	annonces.add(annonce);
	annonce = new Annonce();
	return "";
}
	
	public String removeToOf(){
		offre.remove(annonce);
		annonces.remove(annonce);
		//annonce= new Annonce();
		return "";
	}
	public Annonce getSelectedAnnonce() {
		return selectedAnnonce;
	}
	public void setSelectedAnnonce(Annonce selectedAnnonce) {
		this.selectedAnnonce = selectedAnnonce;
	}
	
	
	}
