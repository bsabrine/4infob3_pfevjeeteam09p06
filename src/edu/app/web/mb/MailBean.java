package edu.app.web.mb;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.app.business.MailServiceLocal;
import edu.app.business.RecrutementCandidateServiceLocal;
import edu.app.business.UserLocal;
import edu.app.persistance.Candidat;
import edu.app.persistance.Qcm;
import edu.app.persistance.RecrutementCandidate;


@ManagedBean
@ViewScoped
public class MailBean {
	
	
	
	@EJB
    private MailServiceLocal msl;
  private UserLocal userLocal;
  private Candidat c=new Candidat();
  private Qcm qcm=new Qcm();
  private int recId;
  
    
    @EJB
    private RecrutementCandidateServiceLocal recruitmentServiceLocal;
    private RecrutementCandidate r=new RecrutementCandidate();
    List<RecrutementCandidate> rCandidates;
    private boolean formDisplayed = false;
	public MailBean() {
		
		
	}
	
	@PostConstruct
	public void init(){
		rCandidates=recruitmentServiceLocal.findAll();
	}

	
	public String send(){
		String navigateTo= null;
		r=recruitmentServiceLocal.findById(recId);
		c=recruitmentServiceLocal.findByCandidat(recId);
		String msg=" Login : "+r.getLogin()+" Password : "+r.getPassword()+"date:"+r.getDate();
		msl.sendMail(c.getMail(), "hello there are your login and password for passing exam in this date .", msg);
		
		return navigateTo;
	}
	public MailServiceLocal getMsl() {
		return msl;
	}

	public void setMsl(MailServiceLocal msl) {
		this.msl = msl;
	}

	
	public RecrutementCandidate getR() {
		return r;
	}

	public void setR(RecrutementCandidate r) {
		this.r = r;
	}

	public List<RecrutementCandidate> getrCandidates() {
		return rCandidates;
	}

	public void setrCandidates(List<RecrutementCandidate> rCandidates) {
		this.rCandidates = rCandidates;
	}

	public RecrutementCandidateServiceLocal getRecruitmentServiceLocal() {
		return recruitmentServiceLocal;
	}

	public void setRecruitmentServiceLocal(
			RecrutementCandidateServiceLocal recruitmentServiceLocal) {
		this.recruitmentServiceLocal = recruitmentServiceLocal;
	}

	public Candidat getC() {
		return c;
	}

	public void setC(Candidat c) {
		this.c = c;
	}

	public Qcm getQcm() {
		return qcm;
	}

	public void setQcm(Qcm qcm) {
		this.qcm = qcm;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}

	public int getRecId() {
		return recId;
	}

	public void setRecId(int recId) {
		this.recId = recId;
	}
	
	
	

}
