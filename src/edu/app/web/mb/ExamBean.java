package edu.app.web.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.app.persistance.Qcm;
import edu.app.persistance.Question;

import edu.app.business.QcmDAOLocal;
import edu.app.business.QuestionDAOLocal;

@ManagedBean(name="examBean")
@SessionScoped
public class ExamBean implements Serializable {
	@EJB
	private QuestionDAOLocal questionDAOLocal;

    
private List <Question> questions= new ArrayList<Question>();
private List <Question> questions2= new ArrayList<Question>();
private Qcm q=new Qcm();
private List<Question> questions3= new ArrayList<Question>();
private List<Qcm> qcms2 = new ArrayList<Qcm>();
	/**
	 * 
	 */
	private static final long serialVersionUID = -2850752395915864664L;
	@EJB
	private QcmDAOLocal qcmDAOLocal;
	
	private List <Qcm> qcms=new ArrayList<Qcm>();
	
	private Qcm qcm = new Qcm();


 @PostConstruct
 public void init () {
	 qcms=qcmDAOLocal.findAllQcms();
	
	 	

		setQuestions(questionDAOLocal.findAllQuestions());
	}
public List <Qcm> getQcms() {
	return qcms;
}


public void setQcms(List <Qcm> qcms) {
	this.qcms = qcms;
}



public QuestionDAOLocal getQuestionDAOLocal() {
	return questionDAOLocal;
}
public void setQuestionDAOLocal(QuestionDAOLocal questionDAOLocal) {
	this.questionDAOLocal = questionDAOLocal;
}

public List<Question> getQuestions() {
	return questions;
}
public void setQuestions(List<Question> questions) {
	this.questions = questions;
}

public List<Question> getQuestions2() {
	return questions2;
}
public void setQuestions2(List<Question> questions2) {
	this.questions2 = questions2;
}
public Qcm getQ() {
	return q;
}
public void setQ(Qcm q) {
	this.q = q;
}
public QcmDAOLocal getQcmDAOLocal() {
	return qcmDAOLocal;
}
public void setQcmDAOLocal(QcmDAOLocal qcmDAOLocal) {
	this.qcmDAOLocal = qcmDAOLocal;
}

public List<Question> getQuestions3() {
	questions3=questionDAOLocal.findbyqcm(qcm);
	
	return questions3;
}
public void setQuestions3(List<Question> questions3) {
	this.questions3 = questions3;
}
public List<Qcm> getQcms2() {
	return qcms2;
}
public void setQcms2(List<Qcm> qcms2) {
	this.qcms2 = qcms2;
}

public Qcm getQcm() {
	return qcm;
}
public void setQcm(Qcm qcm) {
	this.qcm = qcm;
}
public String doSaveOrUpdate(){
 String navigateTo= null;
 qcm.setQuestions(questions2);
 qcmDAOLocal.saveOrUpdateQcm(qcm);

 return navigateTo;
}
}
