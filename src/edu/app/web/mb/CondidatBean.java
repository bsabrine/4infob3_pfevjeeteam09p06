package edu.app.web.mb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.app.business.AnnonceServiceLocal;
import edu.app.business.CandidatServiceLocal;
import edu.app.business.CertificatLocal;
import edu.app.business.CvLocal;
import edu.app.business.DiplomeLocal;
import edu.app.business.ExperienceLocal;
import edu.app.business.FormationLocal;
import edu.app.business.TraineshipLocal;
import edu.app.persistance.Annonce;
import edu.app.persistance.Candidat;
import edu.app.persistance.Certificat;
import edu.app.persistance.Cv;
import edu.app.persistance.Diplome;
import edu.app.persistance.Experience;
import edu.app.persistance.Formation;
import edu.app.persistance.Trainship;

@ManagedBean
@SessionScoped
public class CondidatBean {
	
	@EJB
	CandidatServiceLocal local;
	@EJB
	CvLocal locale;
	@EJB
	TraineshipLocal loc;
	@EJB
	FormationLocal form;
	@EJB
	ExperienceLocal exp;
	@EJB
	CertificatLocal cer;
	@EJB
	DiplomeLocal dip;
	@EJB
	AnnonceServiceLocal offre;
	
	private boolean formDisplayedTrs = false;
	public boolean isFormDisplayedTrs() {
		return formDisplayedTrs;
	}
	public void setFormDisplayedTrs(boolean formDisplayedTrs) {
		this.formDisplayedTrs = formDisplayedTrs;
	}
	
	private boolean formDisplayedFrs = false;
	public boolean isFormDisplayedFrs() {
		return formDisplayedFrs;
	}
	public void setFormDisplayedFrs(boolean formDisplayedFrs) {
		this.formDisplayedFrs = formDisplayedFrs;
	}
	
	private boolean formDisplayedExp = false;
	public boolean isFormDisplayedExp() {
		return formDisplayedExp;
	}
	public void setFormDisplayedExp(boolean formDisplayedExp) {
		this.formDisplayedExp = formDisplayedExp;
	}
	
	private boolean formDisplayedCer = false;
	public boolean isFormDisplayedCer() {
		return formDisplayedCer;
	}
	public void setFormDisplayedCer(boolean formDisplayedCer) {
		this.formDisplayedCer = formDisplayedCer;
	}
	
	private boolean formDisplayedDip = false;
	public boolean isFormDisplayedDip() {
		return formDisplayedDip;
	}
	public void setFormDisplayedDip(boolean formDisplayedDip) {
		this.formDisplayedDip = formDisplayedDip;
	}
	
	private boolean formDisplayedOff = false;
	public boolean isFormDisplayedOff() {
		return formDisplayedOff;
	}
	public void setFormDisplayedOff(boolean formDisplayedOff) {
		this.formDisplayedOff = formDisplayedOff;
	}
	
	Candidat candidat= new Candidat();
	Cv cv = new Cv();
	Trainship trainship= new Trainship();
	Formation formation= new Formation();
	Experience experience= new Experience();
	Certificat certificat= new Certificat();
	Diplome diplome= new Diplome();
	Annonce annonce= new Annonce();
	public Candidat getCandidat() {
		return candidat;
	}
	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}
	
	
	
private List<Trainship> trainshiprs= new ArrayList<Trainship>();
public List<Trainship> getTrainshiprs() {
	return trainshiprs;
}
public void setTrainshiprs(List<Trainship> trainshiprs) {
	this.trainshiprs = trainshiprs;
}

private List<Formation> formations= new ArrayList<Formation>();
public List<Formation> getFormations() {
	return formations;
}
public void setFormations(List<Formation> formations) {
	this.formations = formations;
}

private List<Experience> experiences= new ArrayList<Experience>();
public List<Experience> getExperiences() {
	return experiences;
}
public void setExperiences(List<Experience> experiences) {
	this.experiences = experiences;
}

private List<Certificat> certificats= new ArrayList<Certificat>();
public List<Certificat> getCertificats() {
	return certificats;
}
public void setCertificats(List<Certificat> certificats) {
	this.certificats = certificats;
}

private List<Diplome> diplomes= new ArrayList<Diplome>();
public List<Diplome> getDiplomes() {
	return diplomes;
}
public void setDiplomes(List<Diplome> diplomes) {
	this.diplomes = diplomes;
}

private List<Annonce> annonces= new ArrayList<Annonce>();

public List<Annonce> getAnnonces() {
	annonces= offre.findAll();
	return annonces;
}
public void setAnnonces(List<Annonce> annonces) {
	this.annonces = annonces;
}

	public Cv getCv() {
		return cv;
	}
	public void setCv(Cv cv) {
		this.cv = cv;
	}
	
	
	
	
	public Trainship getTrainship() {
		return trainship;
	}
	public void setTrainship(Trainship trainship) {
		this.trainship = trainship;
	}
	
		
	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	
	
	public Experience getExperience() {
		return experience;
	}
	public void setExperience(Experience experience) {
		this.experience = experience;
	}
	
	
	public Certificat getCertificat() {
		return certificat;
	}
	public void setCertificat(Certificat certificat) {
		this.certificat = certificat;
	}
	
		
	public Diplome getDiplome() {
		return diplome;
	}
	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}
	
	
	public Annonce getAnnonce() {
		return annonce;
	}
	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}
	public String doCreate(){
		cv.setCandidat(candidat);
		cv.addTraineshipsToCv(trainshiprs);
		cv.addFormationsToCv(formations);
		cv.addExperiencesToCv(experiences);
		cv.addCertificatsToCv(certificats);
		cv.addDiplomesToCv(diplomes);
		locale.create(cv);
		return"";
	}
	
	
	public String addToTr(){
		
		trainshiprs.add(trainship);
		trainship= new Trainship();
		return null;
	}
	
public String addToFr(){
		
		formations.add(formation);
		formation= new Formation();
		return null;
	}

public String addToEx(){
	
	experiences.add(experience);
	experience= new Experience();
	return null;
}

public String addToCe(){
	
	certificats.add(certificat);
	certificat= new Certificat();
	return null;
}

public String addToDi(){
	
	diplomes.add(diplome);
	diplome= new Diplome();
	return null;
}

public String addToOf(){
	offre.create(annonce);
	annonces.add(annonce);
	annonce = new Annonce();
	return "";
}
	public String removeToTr(){
		trainshiprs.remove(trainship);
		trainship= new Trainship();
		return null;
	}
	
	public String removeToFr(){
		formations.remove(formation);
		formation= new Formation();
		return null;
	}
	
	public String removeToEx(){
		experiences.remove(experience);
		experience= new Experience();
		return null;
	}
	
	public String removeToCe(){
		certificats.remove(certificat);
		certificat= new Certificat();
		return null;
	}
	
	public String removeToDi(){
		diplomes.remove(diplome);
		diplome= new Diplome();
		return null;
	}
	
	public String removeToOf(){
		offre.remove(annonce);
		annonces.remove(annonce);
		//annonce= new Annonce();
		return "";
	}
	
	
	}
