package edu.app.web.mb;

import java.io.Serializable;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.app.persistance.Qcm;
import edu.app.persistance.Question;
import edu.app.business.QcmDAOLocal;
import edu.app.business.QuestionDAOLocal;
@ManagedBean(name="qcmBean")
@SessionScoped

public class QcmBean implements Serializable  {
	@EJB
	private QuestionDAOLocal questionDAOLocal;
	@EJB
	private QcmDAOLocal qcmDAOLocal;
    
private List <Question> questions;
private Question[] questions2;
private Qcm q=new Qcm();
public Question[] getQuestions2() {
	return questions2;
}
public void setQuestions2(Question[] questions2) {
	this.questions2 = questions2;
}
private Question question;

	private static final long serialVersionUID = 7468621263811736938L;



	public List <Question> getQuestions() {
		return questions;
	}
	
	
	@PostConstruct
	public void init(){
		setQuestions(questionDAOLocal.findAllQuestions());
	}

	public void setQuestions(List <Question> questions) {
		this.questions = questions;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}


	


      


public Qcm getQ() {
	return q;
}
public void setQ(Qcm q) {
	this.q = q;
}
}
