package edu.app.web.mb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.app.business.UserLocal;
import edu.app.persistance.AdminRH;

@ManagedBean
@ViewScoped
public class AdminRhBean {
	@EJB
	private UserLocal userLocal;
	
	private AdminRH adminRH = new AdminRH();
	private List<AdminRH> aRhs;
	private boolean formDisplayed = false;
	
	public AdminRhBean() {
	}
	
	@PostConstruct
	public void init(){
		 aRhs = userLocal.findAllrh();
	}

	public String doSaveOrUpdate(){
		String navigateTo = null;
	userLocal.create(adminRH);
		aRhs = userLocal.findAllrh();
		formDisplayed = false;
		return navigateTo;
	}
	
	public String doNew(){
		String navigateTo = null;
		formDisplayed = true;
		return navigateTo;
	}
	
	public String doCancel(){
		String navigateTo = null;
		adminRH = new AdminRH();
		aRhs = userLocal.findAllrh();
		formDisplayed = false;
		return navigateTo;
	}
	
	public String doDelete(){
		String navigateTo = null;
	userLocal.remove(adminRH);
	aRhs = userLocal.findAllrh();
		formDisplayed = false;
		return navigateTo;
	}

	public AdminRH getAdminRH() {
		return adminRH;
	}

	public void setAdminRH(AdminRH adminRH) {
		this.adminRH = adminRH;
	}

	public List<AdminRH> getaRhs() {
		return aRhs;
	}

	public void setaRhs(List<AdminRH> aRhs) {
		this.aRhs = aRhs;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}
	
	
	

}


